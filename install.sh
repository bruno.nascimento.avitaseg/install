#!/bin/bash
echo "---------------------------------------------------------------------------------------------------------------------"
echo "Atualizando ..."
echo "---------------------------------------------------------------------------------------------------------------------"
sudo apt update -y && \
sudo apt upgrade -y
echo "---------------------------------------------------------------------------------------------------------------------"
echo "Por favor crie seu TOKEN pessoal para acesso aos repositorios do GITLAB via API. Clique em 'Add new token'"
echo "Por favor insira o token de acesso ao Gitlab para baixar as dependencias ..."
echo "Link para gerar token de acesso:"
echo "https://gitlab.com/-/profile/personal_access_tokens?name=GitlabAccessToken&scopes=api,read_api,read_user,read_repository,write_repository,read_registry,write_registry"
echo "login > add new token > create personal access token"
echo "Salve esse token."
echo "---------------------------------------------------------------------------------------------------------------------"
echo "Token de acesso ao GITLAB: "
read PRIVATE_TOKEN

echo "---------------------------------------------------------------------------------------------------------------------"
echo "Instalando Docker"
echo "---------------------------------------------------------------------------------------------------------------------"
curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/48428699/repository/files/install-docker.sh/raw?ref=main" | sh

echo "---------------------------------------------------------------------------------------------------------------------"
echo "Instalando VSCode"
echo "---------------------------------------------------------------------------------------------------------------------"
curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/48428699/repository/files/install-vscode.sh/raw?ref=main" | sh

echo "---------------------------------------------------------------------------------------------------------------------"
echo "Instalando NetCore"
echo "---------------------------------------------------------------------------------------------------------------------"
curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/48428699/repository/files/install-netcore.sh/raw?ref=main" | sh

echo "---------------------------------------------------------------------------------------------------------------------"
echo "Instalando NVM"
echo "---------------------------------------------------------------------------------------------------------------------"
curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/48428699/repository/files/install-nvm.sh/raw?ref=main" | sh

echo "---------------------------------------------------------------------------------------------------------------------"
echo "Instalando DBeaver"
echo "---------------------------------------------------------------------------------------------------------------------"
curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/48428699/repository/files/install-dbeaver.sh/raw?ref=main" | sh

echo "---------------------------------------------------------------------------------------------------------------------"
echo "Instalando Slack"
echo "---------------------------------------------------------------------------------------------------------------------"
curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/48428699/repository/files/install-slack.sh/raw?ref=main" | sh

echo "---------------------------------------------------------------------------------------------------------------------"
echo "Instalando Intune"
echo "---------------------------------------------------------------------------------------------------------------------"
curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/48428699/repository/files/install-intune.sh/raw?ref=main" | sh

echo "---------------------------------------------------------------------------------------------------------------------"
echo "Instalando Postman"
echo "---------------------------------------------------------------------------------------------------------------------"
curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/48428699/repository/files/install-postman.sh/raw?ref=main" | sh

echo "---------------------------------------------------------------------------------------------------------------------"
echo "Instalando Powershell"
echo "---------------------------------------------------------------------------------------------------------------------"
curl --header "PRIVATE-TOKEN: $PRIVATE_TOKEN" "https://gitlab.com/api/v4/projects/48428699/repository/files/install-powershell.sh/raw?ref=main" | sh

echo "---------------------------------------------------------------------------------------------------------------------"
echo "Instalando Gravador de Tela (Simple Screen Recorder)"
echo "---------------------------------------------------------------------------------------------------------------------"
sudo apt-get install simplescreenrecorder

echo "---------------------------------------------------------------------------------------------------------------------"
echo "Instalando YARN"
echo "---------------------------------------------------------------------------------------------------------------------"
npm install --global yarn

echo "---------------------------------------------------------------------------------------------------------------------"
echo "Instalando Angular"
echo "---------------------------------------------------------------------------------------------------------------------"
npm install -g @angular/cli

echo "---------------------------------------------------------------------------------------------------------------------"
echo "Configurando npm ..."
echo "---------------------------------------------------------------------------------------------------------------------"
npm config --userconfig ~/.npmrc set @avita:registry=https://gitlab.com/api/v4/projects/26749643/packages/npm/ && \
npm config --userconfig ~/.npmrc set '//gitlab.com/api/v4/projects/26749643/packages/npm/:_authToken' $PRIVATE_TOKEN

echo "---------------------------------------------------------------------------------------------------------------------"
echo "Instalacao finalizada..."
echo -e "OBS.: Se por algum acaso der erro de permissão no docker,\nrenicie a maquina e rode: docker ps"
echo "---------------------------------------------------------------------------------------------------------------------"
